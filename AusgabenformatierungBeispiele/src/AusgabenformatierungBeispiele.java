
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
/*		System.out.print("Hallo\n");
		System.out.println("Wie geht es dir?");
*/		
//		System.out.printf("|%20s|%n", "123456789");
//		System.out.printf("|%-20s|%n", "123456789");
//		System.out.printf("|%-20.3s|%n", "123456789");
		
//		System.out.printf("|%+-20d|%n", -123456789);
//		System.out.printf("|%20d|%n", -123456789);

		System.out.printf("|%20f|%n", 12345.6789);
		System.out.printf("|%-20f|%n", 12345.6789);
		System.out.printf("|%+-20f|%n", 12345.6789);
		System.out.printf("|%+-20.2f|%n", 12345.6789);		
		
	}

}

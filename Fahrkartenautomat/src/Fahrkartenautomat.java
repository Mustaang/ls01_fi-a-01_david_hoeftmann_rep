﻿import java.util.Scanner;

class Fahrkartenautomat {

	static Scanner tastatur = new Scanner(System.in);
	static String [] fahrkarten = {"Einzelfahrschein Berlin AB","Einzelfahrschein Berlin BC",
							"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB",
							"Tageskarte Berlin BC", "Tageskarte Berlin ABC", "4-Fahrtenkarte Berlin AB",
							"4-Fahrtenkarte Berlin BC", "4-Fahrtenkarte Berlin ABC",
							"4-Fahrtenkarte Kurzstrecke", "Anschlussfahrausweis A/C"};
	static double [] normalpreis = {3.0, 3.5, 3.8, 2.0, 8.8, 9.2, 10.0, 9.4, 12.6, 13.8, 6.0, 1.8};
	static double [] ermäßigtpreis = {1.9, 2.4, 2.7, 1.5, 5.6, 5.9, 6.1, 5.8, 8.6, 9.8, 4.6, 1.4};
	
	
    public static void main(String[] args) {
         
       double zuZahlenderBetrag; 							
       double eingezahlterGesamtbetrag = 0;								
       boolean neustarten = true;
       
       while(neustarten == true) {
    	   willkommen();
    	   
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();																
    	   
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    	   
    	   fahrkartenAusgeben();
       
    	   warten(8);
       
    	   rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
       
    	   System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	  "vor Fahrtantritt entwerten zu lassen!\n"+
    			   			  "Wir wünschen Ihnen eine gute Fahrt.\n");
    	   warten(40);
    	   
    	   System.out.print("\n\n\n\n\n\n\n\n\n");
    	   
       }
    }
        
    
    public static void willkommen() {
    	System.out.println("Willkommen beim Fahrkartenautomaten Ihres Vertrauens!\n");
    	try {
 			Thread.sleep(1500);
        } catch (InterruptedException e) {
 			e.printStackTrace();
 			}
    }
    
    public static double fahrkartenbestellungErfassen() {
    	double zwischensumme = 0.0;
    	double zuZahlenderBetrag = 0.0;
    	int auswahl = 1;
    	
    	do {
    		auswahl = 1;
    		System.out.println("Waehlen Sie eine Fahrkartenart!\n");	// alle Fahrscheine werden aufgelistet
        	System.out.printf("%-5s%-32s%-10s%s%n", "Nr.", "Fahrkarte", "normal", "ermaessigt");
        	System.out.println("----------------------------------------------------------");
        	for (int i=0; i < fahrkarten.length; i++) {  
        		System.out.printf("%-5s%-32s%5.2f%-10s%.2f€%n", "["+auswahl+"]", fahrkarten[i], normalpreis[i], "€", ermäßigtpreis[i]);
        		auswahl++;
        	}
        	System.out.printf("%n%-5s%s%n","[0]", "bezahlen");
        	
    		do {
    			System.out.print("\nIhre Auswahl: ");
    			auswahl = tastatur.nextInt();
        		auswahl -= 1;
        		if (auswahl != -1) {
        			if (auswahl < 0 || auswahl > fahrkarten.length) {	
        				System.out.println("UNGUELTIGE EINGABE!\n");	// bei einer ungültigen Eingabe wird der Benutzer darauf hingewiesen
        			}
        		}
        	} while (auswahl < 0 && auswahl != -1 || auswahl > fahrkarten.length && auswahl != -1);
    									// es wird so lange abgefragt, bis eine gültige Eingabe getätigt wurde
    		
    		if (auswahl != -1) {
    			char norErm;
    			int ermäßigt = 99;
    			String displayErmäßigt = "";
    			do {
    				System.out.println("\nIhre Auswahl: " + fahrkarten[auswahl]);
    				System.out.print("Moechten Sie die Fahrkarte zum Normal- oder zum Ermaessigtenpreis erwerben? (n/e)  ");
    				norErm = tastatur.next().charAt(0);
    				if (norErm == 'n') {
    					ermäßigt = 0;						// "normal oder ermäßigt" wird über eine int gesteuert, da es
    					displayErmäßigt = " normal";		// bei mir mit einem char nicht funktioniert hat
    				} else
    					if (norErm == 'e') {
    						ermäßigt = 1;
    						displayErmäßigt = " ermaessigt";
    					} else {
    						ermäßigt = 99;
    						System.out.println("\nUNGUELTIGE EINGABE!\n");	// bei einer ungültigen Eingabe wird der Benutzer darauf hingewiesen
    					}
    			} while (ermäßigt == 99 );					// es wird so lange abgefragt, bis eine gültige Eingabe getätigt wurde
    	
    			int AnzahlTickets = 0;
    			do {
    				System.out.println("\n\nIhre Auswahl: " + fahrkarten[auswahl] + displayErmäßigt);
    				System.out.print("Anzahl der Tickets (max. 10): ");
    				AnzahlTickets = tastatur.nextInt();
    				if (AnzahlTickets < 1 || AnzahlTickets > 10) {
    					System.out.println("\nUNGUELTIGE ANZAHL DER TICKETS!");	// bei einer ungültigen Eingabe wird der Benutzer darauf hingewiesen

    					System.out.println("Bitte erneut die Anzahl eingeben (1-10)!\n\n");
    				}
    			} while(AnzahlTickets < 1 || AnzahlTickets > 10);		// es wird so lange abgefragt, bis eine gültige Eingabe getätigt wurde

    			
    				if (ermäßigt == 0) {
    					zwischensumme = normalpreis[auswahl] * AnzahlTickets;
    			} else
    				if (ermäßigt == 1) {
    					zwischensumme = ermäßigtpreis[auswahl] * AnzahlTickets;
    				}
    			zuZahlenderBetrag += zwischensumme;
    			System.out.println("\n===================================================="
    								+ "\n\nSie koennen nun eine weitere Fahrkarte kaufen,\n"
    								+ "oder mit dem Bezahlen fortfahren!\n");
    			System.out.printf("%s %.2f€%n%n%s%n%n","Zwischensumme:", zuZahlenderBetrag,"====================================================");
    			try {
    	 			Thread.sleep(3000);
    	        } catch (InterruptedException e) {
    	 			e.printStackTrace();
    	 			}
    		}
    	} while (auswahl != -1);							// wird solange wiederholt, bis "bezahlen" ausgewählt wurde
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	  do {
        	System.out.print("\nNoch zu zahlen: ");
        	System.out.printf("%.2f%s%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " Euro");
        	System.out.print("Eingabe (mind. 5Ct, höchstens 20 Euro) (nur Zahl eingeben, für Cents 0,xx): ");
     	   	eingeworfeneMünze = tastatur.nextDouble();
     	   	if (eingeworfeneMünze != 0.05 && eingeworfeneMünze != 0.1 && eingeworfeneMünze != 0.2 && eingeworfeneMünze != 0.5 && eingeworfeneMünze != 1.0 && eingeworfeneMünze != 2.0 && eingeworfeneMünze != 5.0 && eingeworfeneMünze != 10.0 && eingeworfeneMünze != 20.0) {
     	   		System.out.println("MUENZE / GELDSCHEIN WURDE NICHT ERKANNT!");
     	   	}																	// es werden nur richtige Münzen und Geldscheine erkannt, es wird eine Fehlermeldung ausgegeben
     	  } while (eingeworfeneMünze != 0.05 && eingeworfeneMünze != 0.1 && eingeworfeneMünze != 0.2 && eingeworfeneMünze != 0.5 && eingeworfeneMünze != 1.0 && eingeworfeneMünze != 2.0 && eingeworfeneMünze != 5.0 && eingeworfeneMünze != 10.0 && eingeworfeneMünze != 20.0);
     	   	eingezahlterGesamtbetrag += eingeworfeneMünze;		// duch die Schleife werden falsche Münzen nicht verrechnet
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben\n");
    }
    
    public static void warten(int durchläufe) {
    	for (int i = 0; i < durchläufe; i++) {
           System.out.print("=");
           try {
 			Thread.sleep(250);
        } catch (InterruptedException e) {
 			e.printStackTrace();
 			}
        }
//        System.out.println("\n");
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rückgabebetrag;
    	
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rückgabebetrag = Math.round(rückgabebetrag * 100) / 100.0;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("%s%n%n%s%.2f%s%n", "================================", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) 	// 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
 	          rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;	// Diese Methode wird angewendet, da es sonst zu
            }																// Rundungsfehlern kommt und die Münzen nicht 
            while(rückgabebetrag >= 1.0) 	// 1 EURO-Münzen				// vollständig ausgegeben werden
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
 	          rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;
            }
            while(rückgabebetrag >= 0.5) 	// 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
 	         rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;
            }
            while(rückgabebetrag >= 0.2) 	// 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
  	        rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;
            }
            while(rückgabebetrag >= 0.1) 	// 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
 	         rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;
            }
            while(rückgabebetrag >= 0.05)	// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
  	        rückgabebetrag = Math.round(100.0 * rückgabebetrag) / 100.0;
            }
        } else {
        	System.out.println("================================");
        }
    }
}